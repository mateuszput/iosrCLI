package pl.edu.agh.iosr2.CLI;

import org.junit.Test;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class ConnectionTest
{
    private String targetURL = "https://f4ypo7f5tk.execute-api.eu-west-1.amazonaws.com/prod/number";
    private String targetPOSTURL = "https://lsf9rf7sia.execute-api.eu-west-1.amazonaws.com/prod/resize";
    private URLConnection connection = null;

    @Test
    public void testHttpConnection()
    {
        System.out.println("Starting test 1");
        Boolean responseCorrect = false;
        try
        {
            URL url = new URL(targetURL);
            connection = url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            Integer returnValue = null;

            while((inputLine = in.readLine()) != null)
            {
                System.out.println(inputLine);
                try
                {
                    returnValue = Integer.parseInt(inputLine);

                }
                catch (NumberFormatException e)
                {
                    e.printStackTrace();
                }
            }
            in.close();

            responseCorrect = (returnValue >= 0 && returnValue <= 20);

        } catch (MalformedURLException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        assertTrue(responseCorrect);
    }

    @Test
    public void testJSONConnection2()
    {
        System.out.println("Starting test 2");
        try
        {
            URL url = new URL(targetPOSTURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            String input = "{   \"hello\": \"value3\",   \"testMe\": \"value1\" }";

            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();

            int responseCode = conn.getResponseCode();
            System.out.println("Response code: " + responseCode);

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Output from Server ...");

            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }
            conn.disconnect();
            assertEquals(responseCode, 200);

        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

}