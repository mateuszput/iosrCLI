package pl.edu.agh.iosr2.CLI;

import java.io.*;
import java.util.EnumSet;

public class UserConsole
{
    private boolean userConsoleRunning = true;
    private IAWSImageService serviceHandler;
    private BufferedReader consoleInputReader;

    public UserConsole(IAWSImageService serviceHandler)
    {
        this.serviceHandler = serviceHandler;
        consoleInputReader = new BufferedReader(new InputStreamReader(System.in));
    }

    public void startConsole() throws IOException
    {

        System.out.println("###AWS Image Processing Service Console###");
        while(userConsoleRunning)
        {
            displayMainMenuHint();

            String keyPressed = consoleInputReader.readLine();
            switch (keyPressed)
            {
                case "1":
                    serviceHandler.displayFileList();
                    break;
                case "2":
                    File file = getFileFromUser();
                    if(file != null) serviceHandler.uploadFile(file);
                    break;
                case "3":
                    System.out.println("Please specify file to apply lambda function on:");
                    String lambdaFunctionFile = consoleInputReader.readLine();

                    int i = 0;
                    System.out.println("Please choose lambda function to apply by providing index");
                    for (LambdaFunction info : EnumSet.allOf(LambdaFunction.class)) {
                        System.out.println(i++ + " " + info);
                    }
                    boolean indexAccepted = false;

                    Integer lambdaFunctionIndex = -1;
                    while(!indexAccepted)
                    {
                        try
                        {
                            String userInput = consoleInputReader.readLine();
                            if(userInput.equals("Q")) break;
                            lambdaFunctionIndex = Integer.parseInt(userInput);
                        }
                        catch (NumberFormatException e)
                        {
                            e.printStackTrace();
                        }

                        if(lambdaFunctionIndex >= 0 && lambdaFunctionIndex < LambdaFunction.values().length)
                        {
                            indexAccepted = true;
                        }
                        else
                        {
                            System.out.println("Wrong index! Please try again...");
                        }
                    }
                    if(!indexAccepted) break;
                    serviceHandler.chooseLambda(lambdaFunctionFile, LambdaFunction.values()[lambdaFunctionIndex]);
                    break;
                case "Q":
                    userConsoleRunning = false;
                default:
                    break;
            }
        }
    }

    public void displayMainMenuHint()
    {
        System.out.println("#1 Display list of files on S3");
        System.out.println("#2 Upload file to S3");
        System.out.println("#3 Choose lambda image processing function");
        System.out.println("#Q Quit");
    }

    private File getFileFromUser() throws IOException
    {
        boolean fileExists = false;
        File file = null;
        while(!fileExists)
        {
            System.out.println("Please specify file absolute path");
            String filePath = consoleInputReader.readLine();
            if (filePath.equalsIgnoreCase("Q")) break;
            file = new File(filePath);
            if(file.exists() && !file.isDirectory()) fileExists = true;
            if(!fileExists) System.out.println("Specified file doesn't exist! Please try again or press Q to quit");
        }
        return file;
    }

    public static void main(String[] args)
    {
        AWSService service = new AWSService();

        UserConsole userConsole = new UserConsole(service);
        try
        {
            userConsole.startConsole();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
