package pl.edu.agh.iosr2.CLI;

import java.io.File;

public interface IAWSImageService
{

    void displayFileList();

    void uploadFile(File file);

    void chooseLambda(String file, LambdaFunction lambdaFunction);

}
