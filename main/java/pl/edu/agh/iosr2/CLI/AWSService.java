package pl.edu.agh.iosr2.CLI;

import java.io.File;


public class AWSService implements IAWSImageService
{
    @Override
    public void displayFileList()
    {
        System.out.println("Displaying file list...");
    }

    @Override
    public void uploadFile(File file)
    {
        System.out.println("Uploading file " + file + " to S3...");

    }

    @Override
    public void chooseLambda(String file, LambdaFunction lambdaFunction)
    {
        System.out.println("Running lambda function... " + lambdaFunction + " for file: " + file);
    }
}
